
/* This reducer handles any navigation values

Products: Initially this is set to null but once the user selects a collection a request will be made for the products in the given collection in the form of a js object.

	{
		...given by server,
		{
			...for each given product,
			collapsed: boolean, default true,
			variationId: int, default null, // this is the id of the variation to show
		}
	}


*/

import {CONST_INIT_PRODUCTS, CONST_GET_PRODUCTS, CONST_GET_PRODUCTS_ID, CONST_SINGLE_PRODUCT_SELECTION } from "../action/products";

export function products(state = {}, action) {
	switch (action.type) {
		case CONST_INIT_PRODUCTS:
			return null;
		case CONST_GET_PRODUCTS:
			return {
				...state,
				productsObj: action.dataObj,
				isLoading: false,
			};
		case CONST_GET_PRODUCTS_ID:
			return {
				productIds: action.dataObj,
				isLoading: true,
			};
		case CONST_SINGLE_PRODUCT_SELECTION:
			// In this case it's a selection from the recent sales, this is either a null or an int
			return {
				...state,
				singleProductId: action.dataObj,
			};
		default:
			return state;
	}

}