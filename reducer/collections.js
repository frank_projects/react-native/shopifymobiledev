
/* This reducer handles any navigation values

Collections:  js object of the collections that the user has. This will be requested at the launch of the app.
	{
		...given by server,
	}

*/

import {CONST_INIT_COLLECTIONS, CONST_LOAD_COLLECTIONS} from "../action/collections";

export function collections(state = {}, action) {
	switch (action.type) {
		case CONST_INIT_COLLECTIONS:
			return null;
		case CONST_LOAD_COLLECTIONS:
			return {
				...action.dataObj
			};
		default:
			return state;
	}

}