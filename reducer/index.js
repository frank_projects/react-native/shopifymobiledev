import { combineReducers} from "redux";
import { collections } from "./collections";
import { products } from "./products";
import { nav } from "./nav";

/* This compiles all three of the possible states together and ensures redux handles all of them*/
export default combineReducers({
  collections: collections,
	products: products,
	nav: nav,
})