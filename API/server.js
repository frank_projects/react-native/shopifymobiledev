export const collectionsURL = "https://shopicruit.myshopify.com/admin/custom_collections.json?page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6";

export function getURLProduct(idArray) {
	/* idArray is an array of ints
	this return the url to display certain products*/
	return `https://shopicruit.myshopify.com/admin/products.json?ids=${idArray}&page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6`;
}

export function getURLProductList(id) {
	/* this returns the url to request the list of products */
	return `https://shopicruit.myshopify.com/admin/collects.json?collection_id=${id}&page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6`
}