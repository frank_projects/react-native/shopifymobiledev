/* This is the shared actions that can occur*/
import { initCollections } from "./collections";
import { initNav } from "./nav";
import { initProducts } from "./products";
export function initialLoad(){
	return (dispatch) => {
		dispatch(initProducts());
		dispatch(initNav());
		dispatch(initCollections());
	}
}