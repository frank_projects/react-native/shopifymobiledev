import { getURLProductList, getURLProduct } from "../API/server";

export const CONST_INIT_PRODUCTS = "CONST_INIT_PRODUCTS";
export const CONST_GET_PRODUCTS = "CONST_GET_PRODUCTS";
export const CONST_GET_PRODUCTS_ID = "CONST_GET_PRODUCTS_ID";
export const CONST_SINGLE_PRODUCT_SELECTION = "CONST_SINGLE_PRODUCT_SELECTION";
export function initProducts(){
	return {
		type: CONST_INIT_PRODUCTS
	}
};



export function getProductsId(id){
	// get the products within a collections
	return (dispatch) => {
		fetch(getURLProductList(id))
				.then((response) => response.json())
				.then(responseJSON => {
					return dispatch(addProductsId(responseJSON));
				})
				.catch((error) => {
					console.error(error);
				});
	}
}

function addProductsId(dataObj) {
	return {
		type: CONST_GET_PRODUCTS_ID,
		dataObj: dataObj,
	}
}

export function getProducts(idArray){
	// get the collections
	return (dispatch) => {
		fetch(getURLProduct(idArray))
				.then((response) => response.json())
				.then(responseJSON => {
					return dispatch(addProducts(responseJSON));
				})
				.catch((error) => {
					console.error(error);
				});
	}
}

function addProducts(dataObj) {
	return {
		type: CONST_GET_PRODUCTS,
		dataObj: dataObj,
	}
}

export function addSingleProductId(id){
	return {
		type: CONST_SINGLE_PRODUCT_SELECTION,
		dataObj: id,
	}
}