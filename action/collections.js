export const CONST_INIT_COLLECTIONS = "CONST_INIT_COLLECTIONS";
export const CONST_LOAD_COLLECTIONS = "CONST_LOAD_COLLECTIONS";
import {collectionsURL} from "../API/server";

export function initCollections() {
	return {
		type: CONST_INIT_COLLECTIONS
	}
}

export function getCollections() {
	// get the collections
	return (dispatch) => {
		fetch(collectionsURL)
				.then((response) => response.json())
				.then(responseJSON => {
					return dispatch(addCollections(responseJSON));
				})
				.catch((error) => {
					console.error(error);
				});
	}
}

function addCollections(dataObj) {
	return {
		type: CONST_LOAD_COLLECTIONS,
		dataObj: dataObj,
	}
}

