import React from "react";
import {Text, CardItem} from "native-base";
import {View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {material} from 'react-native-typography'
import CircularImage from '../CircularImage'
import Icon from 'react-native-vector-icons/FontAwesome';
import {getProductsId} from "../../../action/products";
import {withNavigation} from 'react-navigation';

/* Expects a collection object as prop containing:
* {
	"id": 68424466488,
	"title": "Aerodynamic collection",
	"body_html": "The top of the line of aerodynamic products all in the same collection.",
	"image": {
		"src": "https://cdn.shopify.com/s/files/1/1000/7970/collections/Aerodynamic_20Cotton_20Keyboard_grande_b213aa7f-9a10-4860-8618-76d5609f2c19.png?v=1545072718"
		}
}
*/
class CategoryCard extends React.Component {
	render() {
		const {collection} = this.props;
		const imageUrl = collection.image.src;

		return (
				<CardItem bordered key={`collection-id:${collection.id}`}>
					<TouchableOpacity
							onPress={() => this.handleCollectionSelect()}>
						<View style={Styles.imageRight}>
							{/* The image to the left */}
							<View style={{width: 70}}>
								<CircularImage url={imageUrl}/>
							</View>
							<View style={{width: 210}}>
								{collectionAndBody(collection.title, collection.body_html)}
							</View>
							<View style={Styles.centerVertically}>
								<Icon size={42} name='angle-right' styles = {{color: '#CCD0DB'}}/>
							</View>
						</View>
					</TouchableOpacity>
				</CardItem>
		);
	}

	handleCollectionSelect() {
		this.props.dispatch(getProductsId(this.props.collection.id));
		this.props.navigation.navigate("ProductListSyncingComponent");
	}
}


function collectionAndBody(title, body) {
	/* title: String
	*  body: String
	*  Return's a View with the proper styling*/

	let bodyContent = body;
	if (bodyContent === "") {
		bodyContent = "Hmm, no description here."
	}
	return (
			<View style={Styles.titleAndDescription}>
				<Text style={material.body1}>{title}</Text>
				<Text style={material.caption}>{bodyContent}</Text>
			</View>
	)

}


const Styles = {
	centerVertically: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	imageRight: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'stretch'
	},
	titleAndDescription: {
		flex: 1,
		flexDirection: 'column',
		paddingTop: 20,
	}
};


export default connect()(withNavigation(CategoryCard));
