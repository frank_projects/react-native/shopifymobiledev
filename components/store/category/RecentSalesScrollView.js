import React, {Component} from 'react';
import {ScrollView, TouchableOpacity} from 'react-native';
import {CONST_RECENT_SALES_URLS, getRobots} from "./RecentSalesStatic";
import CircularImage from "../CircularImage";
import { getProducts} from "../../../action/products";
import {connect} from "react-redux";
import {withNavigation} from "react-navigation";
/* This is currently meant for demoing only so it's not actually
* calling an API for anything.*/
class RecentSalesScrollView extends Component {
	constructor(props) {
		super(props);
		this.robotsArray = getRobots();
	}

	render() {
		return (
				<ScrollView
						horizontal={true}
				>
					{
						this.robotsArray.map((robotString, i) => {
							if (robotString === "") {
								return null
							}
							return (
									<TouchableOpacity
											key={`RecentSalesPhotoTouch-index:${i}`}
											onPress={() => this.handleTouch(robotString.split(",")[1])}>
										<CircularImage
												url={robotString.split(",")[0]}
												key={`RecentSalesPhoto-index:${i}`}
										/>
									</TouchableOpacity>
							)
						})
					}
				</ScrollView>
		);
	}

	handleTouch(productId) {
		this.props.dispatch(getProducts([parseInt(productId)]));
		this.props.navigation.navigate("ProductListSyncingComponent");

	}
}

export default connect()(withNavigation(RecentSalesScrollView));