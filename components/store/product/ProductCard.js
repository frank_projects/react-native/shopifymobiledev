import React from 'react';
import {Text, CardItem, Card} from "native-base";
import {View, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import {material} from 'react-native-typography'
import CircularImage from '../CircularImage'
import Icon from 'react-native-vector-icons/FontAwesome';
import {getProductsId} from "../../../action/products";
import {withNavigation} from 'react-navigation';
import ProductCardCollapsed from "./ProductCardCollapsed";
import ProductCardUncollapsed from "./ProductCardUncollapsed"

const STATE_COLLAPSED = "STATE_COLLAPSED";
const STATE_UNCOLLAPSED = "STATE_UNCOLLAPSED";

class ProductCard extends React.Component {
	state = {mode: STATE_COLLAPSED};

	// state = {mode: STATE_UNCOLLAPSED}; // TODO change this back
	constructor(props) {
		super(props);


		this.totalInventory = inventoryLeft(this.props.product.variants);
		this.amountSold = random(Math.round(this.totalInventory / 8), Math.round(this.totalInventory / 3));

	}

	render() {
		return (
				<CardItem bordered key={`product-card-id:${this.props.index}`}>
					{
						this.state.mode === STATE_COLLAPSED
								? <ProductCardCollapsed
										changeCardState={this.changeCardState.bind(this)}
										product={this.props.product}
										totalInventory={this.totalInventory}
										amountSold={this.amountSold}
								/>
								: <ProductCardUncollapsed
										changeCardState={this.changeCardState.bind(this)}
										product={this.props.product}
										totalInventory={this.totalInventory}
										amountSold={this.amountSold}
								/>
					}
				</CardItem>
		);
	}

	changeCardState() {
		if (this.state.mode === STATE_COLLAPSED) {
			this.setState({mode: STATE_UNCOLLAPSED})
		} else { // mode is not collapsed so change it to collapsed
			this.setState({mode: STATE_COLLAPSED})
		}
	}
}

function inventoryLeft(productVarients) {
	let accum = 0;
	for (let i = 0; i < productVarients.length; i++) {
		accum += productVarients[i].inventory_quantity;
	}
	return accum;
}

export function random(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


export default ProductCard;