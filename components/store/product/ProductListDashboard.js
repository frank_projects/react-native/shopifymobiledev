import React from 'react';
import {ScrollView, TouchableOpacity, View, FlatList} from 'react-native';
import {CardItem, Container, Content, Text, Card} from "native-base";
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {material} from 'react-native-typography'
import {connect} from "react-redux";
import Loading from "../../Loading";
import {getProducts} from "../../../action/products";
import ProductCard from "./ProductCard"

class ProductListDashboard extends React.Component {

	render() {
		return (
				<Container>
					<Content>
						{this.renderProductIds()}
					</Content>
				</Container>)
	}

	renderProductIds() {
		return (
				<View style={{flex: 1}}>
					{
						this.props.productsArray
								? <FlatList
										data={this.props.productsArray}
										renderItem={
											({item, index}) =>
													<ProductCard
															key={`ProductCard-Index:${index}`}
															product={item}
															index={index}
													/>
										}
										style={{flexGrow: 1}}
										keyExtractor={(item, index) => index.toString()}
								/>
								: null
					}
				</View>
		)
	}

}

const Styles = {
	backButton: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10
	},
};

function mapStateToProps({products}) {
	let listProducts = [];
	if (
			products !== undefined &&
			products.productsObj !== undefined &&
			products.productsObj.products !== undefined
	) {
		listProducts = products.productsObj.products;
	}
	/* This will give an array of product objects*/
	return {productsArray: listProducts}
}


export default connect(mapStateToProps)(withNavigation(ProductListDashboard));