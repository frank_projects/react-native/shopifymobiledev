import React from 'react';
import * as Progress from 'react-native-progress';
/* Expects as props a float of the progress and a width side*/
export default class ProgressBar extends React.Component {
	render(){
		return(
				<Progress.Bar
						progress={this.props.progress}
						width={this.props.width}
						color = {'rgb(94,107,199)'}
				/>
		)
	}
}