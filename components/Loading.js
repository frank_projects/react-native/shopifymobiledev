import React from 'react';
import {Text, Container} from "native-base";

/* This is the loading animation that will be used for all screen requiring async
* with networks or requires a lot of processing*/

export default class Loading extends React.Component {
	render() {
		return (
				<Container style = {textStyle}>
					<Text>Loading...</Text>
				</Container>
		)
	}
}

const textStyle = {
	flex: 1,
	alignItems: 'center'
};