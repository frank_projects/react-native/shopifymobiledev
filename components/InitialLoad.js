import React from 'react';
import AppNavigator from "../navigation/AppNavigator";
import {connect} from "react-redux";
import {initialLoad} from "../action/shared";
import {initCollections} from "../action/collections";
import {initNav} from "../action/nav";
import {initProducts} from "../action/products";
import { getCollections } from "../action/collections";

/* This loads the initial data */
class InitialLoad extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
  	// const hasData = this.props.navigation.collectionId !== undefined;
	  const hasData = false;
		// if collections has not been loaded then it means that there was no previous state
	  if (!hasData){
	  	this.props.dispatch(initNav());
	  }
	  this.props.dispatch(getCollections());
  }
  render(){return(<AppNavigator/>)};


}
function mapStateToProps({ navigation }) {
  	return { navigation: navigation };
}


export default connect(mapStateToProps)(InitialLoad);