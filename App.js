import React from 'react';
import {Platform, StatusBar, StyleSheet, View} from 'react-native';
import {AppLoading, Asset, Font, Icon} from 'expo';
import InitialLoad from "./components/InitialLoad";
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducer from './reducer';
import middleware from './middleware';
let store = createStore(reducer, middleware);

export default class App extends React.Component {
	state = {
		isLoadingComplete: false,
	};

	render() {
		if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
			return (
					<AppLoading
							startAsync={this._loadResourcesAsync}
							onError={this._handleLoadingError}
							onFinish={this._handleFinishLoading}
					/>
			);
		} else {
			return (
					<Provider store={store}>
							<View style={styles.container}>
								{Platform.OS === 'ios' &&
								<StatusBar backgroundColor = "#5E6BC7" barStyle="default"/>}
								 <InitialLoad />
							</View>
					</Provider>
			);
		}
	}

	_loadResourcesAsync = async () => {
		return Promise.all([
			Asset.loadAsync([
				require('./assets/images/robot-dev.png'),
				require('./assets/images/robot-prod.png'),
			]),
			Font.loadAsync({
				// This is the font that we are using for our tab bar
				...Icon.Ionicons.font,
				// We include SpaceMono because we use it in Store.jsl free
				// to remove this if you are not using it in your app
				'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
				Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
			}),
		]);
	};

	_handleLoadingError = error => {
		// In this case, you might want to report the error to your error
		// reporting service, for example Sentry
		console.warn(error);
	};

	_handleFinishLoading = () => {
		this.setState({isLoadingComplete: true});
	};
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
});
