import React from 'react';
import {
	createAppContainer,
	createSwitchNavigator,
	createStackNavigator,
	createDrawerNavigator,

} from 'react-navigation';
import MainTabNavigator from './MainTabNavigator';
import SideBar from "../components/Sidebar";
import {
	Button,
	Icon,
	View,
	Text,
} from "native-base";
import {Image} from 'react-native';
import shopifylogo from "../img/shopifylogo.png";
import ProductListSyncingComponent from "../components/store/product/ProductListSyncingComponent";
import StoreScreen from "../components/StoreScreen"

const drawerButton = (
		<View style={{marginTop: 2}}>
			<Button
					transparent
					onPress={() => this.props.navigation.navigate("SideBar")}
			>
				<Icon name="menu"/>
			</Button>
		</View>
);

const alertButton = (
		<View style={{marginTop: 2}}>
			<Button
					transparent
			>
				<Icon name="ios-notifications-outline"/>
			</Button>
		</View>
);
const shopifyLogo = (
		<Image
				style={{width: 150, height: 40}}
				source={{uri: 'https://i.imgur.com/7IMnxw9.png'}}/>
);
// this is the left side of the navigation bar
const headerLeftMain = (
		<React.Fragment>
			{ shopifyLogo }
		</React.Fragment>
);
const HomeStackNavigator = createStackNavigator(
		{
			// You could add another route here for authentication.
			// Read more at https://reactnavigation.org/docs/en/auth-flow.html
			Main: {
				screen: MainTabNavigator,
				navigationOptions: { header: null }
			},
			SideBar: {
				screen: SideBar
			},
			ProductListSyncingComponent: {
				screen: ProductListSyncingComponent
			},


		},
		{
			backBehavior: 'initialRoute',
			initialRouteName: 'Main',
		});

const DrawerNavigator = createDrawerNavigator(
		{
			Home: {
				screen: HomeStackNavigator,
			},
			SideBar: {
				screen: SideBar,
			},
			ProductListSyncingComponent: {
				screen: ProductListSyncingComponent
			},
			CategoryDashboard:{
				screen: HomeStackNavigator
			}
		},
		{
			backBehavior: 'initialRoute',
			contentComponent: props => <SideBar {...props} />
		}
);


export default createAppContainer(DrawerNavigator);