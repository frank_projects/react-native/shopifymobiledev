import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import StoreScreen from '../components/StoreScreen';
import LinksScreen from '../components/LinksScreen';
import SettingsScreen from '../components/SettingsScreen';

const HomeStack = createStackNavigator({
	Home: StoreScreen,
});

HomeStack.navigationOptions = {
	tabBarLabel: 'Store',
	tabBarIcon: ({focused}) => (
			<TabBarIcon
					focused={focused}
					name={
						Platform.OS === 'ios'
								? `ios-information-circle${focused ? '' : '-outline'}`
								: 'md-information-circle'
					}
			/>
	),
};

const LinksStack = createStackNavigator({
	Links: LinksScreen,
});

LinksStack.navigationOptions = {
	tabBarLabel: 'Links',
	headerStyle: {
      backgroundColor: '#5E6BC7',
    },
	tabBarIcon: ({focused}) => (
			<TabBarIcon
					focused={focused}
					name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
			/>
	),
};

const SettingsStack = createStackNavigator({
	Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
	tabBarLabel: 'Settings',
	tabBarIcon: ({focused}) => (
			<TabBarIcon
					focused={focused}
					name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
			/>
	),
};
const bottomTabConfig = {
	tabBarOptions: {
		activeTintColor: 'white',
		labelStyle: {
			fontSize: 12,
		},
		style: {
			backgroundColor: "#5E6BC7",
		},
	}
};
export default createBottomTabNavigator({
	HomeStack,
	LinksStack,
	SettingsStack,
}, bottomTabConfig);
