# Project Purpose:
This is an app demo created by Frank Haolun Li  and as part of a coding challenge by Shopify. The task was to use Shopify's merchant store API to create a merchant store. As extra specification in the challenge is to build it within a framework that can be compiled natively to both IOS and Android. 

For my design there are four major features of the app:
1. Merchants will be able to see their most popular sale dynamically updated in their main screen. 
2. Merchants will be able to see categories for which their products are organized into. 
3. Merchants will be able to tap into a particular category to see a list of all the products.
4. Merchants can tap into any particular product important details such as how much product is sold compared with how much is currently in stock, the money made by purchases, the cost of each item, ..etc. 


# Demo 
## See popular sale dynamically updated in their main screen
![alt text](https://gitlab.com/frank_projects/react-native/shopifymobiledev/raw/master/markdown/1%20Top%20Scroll.gif "demo1")

## See details of each product
![alt text](https://gitlab.com/frank_projects/react-native/shopifymobiledev/raw/master/markdown/3%20merchant.gif "demo3")



# Planning
Below I documented my workflow and steps I went through to create this project. I believe that the steps a developer takes is just as important as the end product. 


## Planning 1: Rough sketch of design
![alt text](https://gitlab.com/frank_projects/react-native/shopifymobiledev/raw/master/markdown/sketchPlanning.jpeg "rough sketch planning")

## Planning 2: Specify how data will interact within the app
The purpose here is to consider how data will be stored and handled within the app. 

The choice here is to use Redux. Although this project is simple to justify the use of Redux I made the choice to use it to demonstrate familiarity with managing data in a concise way.

Each time the app opens the data is reloaded and refreshed.The reason why we use ids instead of index is in case the ordering is changed or something is deleted

The data stored on the phone will contain the following

### Collections
Collections:  js object of the collections that the user has. This will be requested at the launch of the app.
{
...given by server,
}
### Products

Products: Initially this is set to null but once the user selects a collection a request will be made for the products in the given collection in the form of a js object. 

{
...given by server,
{
	...for each given product,
	collapsed: boolean, default true, 
	variationId: int, default null, // this is the id of the variation to show 
}
}

### Navigation
{
	collectionId: null by default otherwise it's an int of the collection selected,

}

## Planning 3: Detailed plan for each component

### Sidebar
Component Name: Sidebar
Required Data: None
Parent Component Name: None. This is static and for demonstration purposes only.
Child Component Name: CategoryDashboard
Description:This the sidebar and top bar for the app. 

### Category Screen
Component Name: CategoryDashboard
Required Data: Collections data requested from the server
Parent Component Name: CategoryDashboard
Child Component Name: RecentSales, CategoryCard
Description: This is the screen for the Categories View. 

Component Name: RecentSales
Required Data: None. This is static and for demonstration purposes only.
Parent Component Name: Category Dashboard 
Child Component Name: 
Description: Displays the most recent sales in the form of small icons that is scrollable horizontally

Component Name:CategoryCard
Required Data: index: int index of the product selected
Parent Component Name: CategoryDashBoard
Child Component Name: None
Description:Component displays a single category with an icon to the left, title and description to the right. Tapping on the icon will navigate to ProductsDashboard 

### Product Screen
Component Name: ProductDashboard
Required Data: The data for all the products for a given category
Parent Component Name: Sidebar
Child Component Name: ProductCard
Description: This is the Product Screen that displays the information for the data 

Component Name: ProductCard
Required Data: index: int index of the product selected
Parent Component Name: ProductDashboard
Child Component Name: ProductCollapsed, ProductUncallopsed
Description: This is the card that will switch between ProductCollapsed and ProductUncollapsed. This should check Redux state for whether or not it's collapsed in 

Component Name: ProductCollapsed
Required Data: index: int index of the product selected
Parent Component Name: ProductDashboard
Child Component Name: StatusBar.js
Description: This is the collapsed form for a given product. It will be simpler containing only the product name, an icon, and a progress bar showing how much is sold.

Component Name: ProductCallopsed
Required Data: index: int index of the product selected 
Parent Component Name: ProductCard
Child Component Name: StatusBar
Description: This component is the uncollapsed form which contains more information about a given product and an indicator to show which product is being viewed. This component is scrollable horizontally.


## Planning 4 :Order of operations when programming and timeline
Stage 2, Planning 5, Order of operations when programming and Timeline

It's important to get certain things off the ground first because some of it will decide the logical structure of everything else.

I will also plan out sub goals and aim to reach them leaving 2 days before the absolute deadline as cushion.

Timeline
Hard Deadline: January 21st at 9:00am EST
Aim to Finish By: January 19th at 9:00 am EST


I will complete the project from top to bottom of this list


✅ Set up empty redux store with middleware that prints out the state

✅ Set up storing the redux store 

✅ Set up fetching data for category 

✅ Set up fetching data for product list 

✅ Sidebar.js 

✅ Set up stack navigation with a working back button 

✅ CategoryDashboard.js 

✅ RecentSales.js 

✅ CategoryCard.js 

✅ Test Categories Screen

✅ ProductDashboard.js 

✅ ProductCard.js 

✅ ProductCollapsed.js 

✅ StatusBar.js 

✅ ProductUncallopsed.js 

✅Test Product Screen 

✅ Compile APP to native Android APK 

# How to Load the App via Expo
The project uses Expo and the Create-React-Native-App starter.  

```
git clone https://gitlab.com/Frank_React/shopifymobiledev.git
npm install
```
Once all of the dependencies have been installed you can launch the app with
```
expo start
```

A new browser window should automatically open displaying the app.  If it doesn't, navigate to [http://localhost:19002/](http://localhost:19002/) in your browser

## Android Native Build 
You can access the Android APK by here:
https://gitlab.com/Frank_React/shopifymobiledev/blob/master/Android%20APK/mobileshopify%20apk%20V1.1.1.apk


### Resources and Documentation:
* [Create-react-app Documentation](https://github.com/facebookincubator/create-react-app)
* [React Router Documentation](http://knowbody.github.io/react-router-docs/)
* [React Training/React Router](https://reacttraining.com/react-router/web/api/BrowserRouter)
* [React API](https://facebook.github.io/react/docs/react-api.html)

### Udacity Resources:
* [Project starter template](https://github.com/udacity/reactnd-project-myreads-starter)
* [Project Rubric](https://review.udacity.com/#!/rubrics/918/view)
* [Udacity CSS Style Guide](http://udacity.github.io/frontend-nanodegree-styleguide/css.html)
* [Udacity HTML Style Guide](http://udacity.github.io/frontend-nanodegree-styleguide/index.html)
* [Udacity JavaScript Style Guide](http://udacity.github.io/frontend-nanodegree-styleguide/javascript.html)
